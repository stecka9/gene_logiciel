export default class All {
    constructor(  nom,  prenom,  cin, email, mobile, role, date_de_naissance, lieu_de_naissance) {
      
      this.nom = nom;
      this.prenom = prenom;
      this.cin = cin;
      this.email = email;
      this.mobile = mobile;
      this.role = role;
      this.date_de_naissance = date_de_naissance;
      this.lieu_de_naissance = lieu_de_naissance;
  }
}