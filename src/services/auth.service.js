import axios from 'axios';

const API_URL = 'http://localhost:8084/api/v1/user/';

class AuthService {
  login(user) {
console.log(user.password);
console.log(user.username);
    return axios.post(API_URL + 'SignIn', {
        "login": user.username,
        "passwd": user.password
      })
      .then(response => {
        if (response.data) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();
